# Debian base renders an image of 958MB
# Alpine base renders an image of 425MB
# Alpine base with multistage build renders an image of 80MB,
#   without any compilers included, improving the security posture

ARG ALPINE_IMAGE=python
ARG ALPINE_VERSION=3.11-alpine

FROM ${ALPINE_IMAGE}:${ALPINE_VERSION} as compile-image
WORKDIR /app

# Installing necessary dependencies for Flask
RUN apk --no-cache add  \
            python3-dev=3.10.8-r3 \
            build-base=0.5-r3 \
            linux-headers=5.19.5-r0 \
            pcre-dev=8.45-r2 \
# Create user python to run the app as non-root
  && adduser -D python \
  && chown python:python -R /app

USER python

ENV PYTHONPATH="./app" \
    PATH="${PATH}:/home/python/.local/bin"

COPY --chown=python:python requirements.txt ./
COPY --chown=python:python app ./

RUN pip3 install --no-cache-dir --user -r requirements.txt

FROM ${ALPINE_IMAGE}:${ALPINE_VERSION} as app-image

WORKDIR /app

# pcre-dev still needed for shared library by uwsgi
RUN apk --no-cache add  \
            pcre-dev=8.45-r2 \
  && adduser -D python \
  && chown python:python -R /app

USER python

ENV PYTHONPATH="./app" \
    PATH="${PATH}:/home/python/.local/bin"

COPY --from=compile-image /home/python/.local /home/python/.local
COPY --from=compile-image /app /app

EXPOSE 8600

ENTRYPOINT ["/home/python/.local/bin/uwsgi" , "--need-app"]
CMD ["app.ini"]
