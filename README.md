# README

## Introduction

This repository contains:

- A dockerization of a minimal flask application, taken from [https://github.com/matdoering/minimal-flask-example](https://github.com/matdoering/minimal-flask-example). The Dockerfile is in the root of the project, the sourcecode for the app is located in the `app` folder.
- A deployment for deploying the minimal flask application on Kubernetes, all located in the `k8s` folder. The app is not that complex that templating with e.g. Helm would be strictly needed. However, templating _was_ used to show a possible approach; Kustomize was used for this. It is possible to deploy the flask app to both a test- and production environment. Only in the latter the deployment can be updated in a highly-available way.

## Setup

The reader is expected to have [minikube](https://minikube.sigs.k8s.io/docs/start/) installed and running in his environment. Make sure that your `kubectl` config points to the minikube cluster and lastly make sure that you can access minikube Loadbalancer services by running `minikube tunnel`.

In order to deploy the minimal flask app to the `staging` environment:

```
kubectl apply -k https://gitlab.com/n13k77/minimal-flask.git/k8s/overlays/staging/
```

In order to deploy the minimal flask app to the `prod` environment:
```
kubectl apply -k https://gitlab.com/n13k77/minimal-flask.git/k8s/overlays/prod/
```
To check the deployment:
```
MINIMAL_FLASK_IP=`kubectl get svc -n prod | grep -v "IP" | awk '{ print $4 }'`
curl http://$MINIMAL_FLASK_IP:8600/health
```
This should return `OK`, indicating that the service is running. Replace `prod` with `test` to check the test deployment.
